package by.minsk.itacademy.lesson7;

interface List<Element> extends Iterable<Element> {

    void add(Element element);
    void remove(int index)throws ArrayIndexOutOfBoundsException;
    Element get(int index) throws ArrayIndexOutOfBoundsException;
    Element set(int index, Element element);

}
