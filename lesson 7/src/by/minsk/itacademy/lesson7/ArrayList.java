package by.minsk.itacademy.lesson7;

import java.util.Iterator;

public class ArrayList<Element> implements List<Element> {
    private Element[] array;
    private static final int DEFAULT_CAPACITY = 10;

    public ArrayList() {
        array = (Element[]) new Object[DEFAULT_CAPACITY];
    }

    public Element[] getArray() {
        return array;
    }

    public ArrayIterator iterator() {
        return new ArrayIterator(this);
    }

    public int getLength() {
        return array.length;
    }

    public int getCapacity() {
        return array.length;
    }

    @Override
    public void add(Element element) {
        int a = 0;
        int i = 0;
        while (a == 0 & i < array.length) {
            if (array[i] == null) {
                array[i] = element;
                a = 1;
            }
            i++;
        }

        if (a == 0) {
            Element[] tempArray = (Element[]) new Object[(array.length + 1)];
            System.arraycopy(array, 0, tempArray, 0, array.length);
            tempArray[array.length] = element;
            array = tempArray;
        }
    }


    @Override
    public Element get(int index) {
        if (index < array.length) {
            return array[index];
        } else throw new ArrayIndexOutOfBoundsException(index);
    }

    @Override
    public void remove(int index) {
        if (0 <= index & index < array.length) {
            Element[] tempArray = (Element[]) new Object[array.length - 1];
            System.arraycopy(array, 0, tempArray, 0, (index));
            System.arraycopy(array, index + 1, tempArray, index, array.length - index - 1);
            array = tempArray;
        } else throw new ArrayIndexOutOfBoundsException(index);
    }

    @Override
    public Element set(int index, Element element) {
        Element oldElement = array[index];
        array[index] = element;
        return oldElement;
    }

    public void printOut() {
        for (Element i : array) System.out.print(i+" ");
    }

}




