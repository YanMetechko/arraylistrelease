package by.minsk.itacademy.lesson7;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<Element> implements Iterator<Element> {

    private ArrayList<Element> sample;
    private int index = 0;

    public ArrayIterator(ArrayList<Element> a) {
        this.sample = a;
    }

    @Override
    public boolean hasNext() {
        if (0 <= index & index < (sample.getLength() - 1)) {
            return true;
        }
        return false;
    }

    @Override
    public Element next() {
        if (0 <= index & index < sample.getLength()) {
            index++;
            return sample.get(index);
        }
        throw new NoSuchElementException("No more elements left");
    }


}
