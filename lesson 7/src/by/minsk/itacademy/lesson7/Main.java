package by.minsk.itacademy.lesson7;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> trial = new ArrayList<>();
        for (int i = 0; i < 21; i++) {
            trial.add((int)(Math.random() * 100));
        }
        ArrayIterator iterator = new ArrayIterator(trial);
        System.out.print(trial.get(0)+" ");
        while(iterator.hasNext())
            System.out.print(iterator.next()+" ");
        System.out.println();
        trial.remove(11);
        trial.printOut();

    }
}
